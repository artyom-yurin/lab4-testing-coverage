package com.hw.db.DAO;

import com.hw.db.models.User;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class userDAOTests {
    private JdbcTemplate mockJdbc;
    private String stubNickname;
    private String stubEmail;
    private String stubFullname;
    private String stubAbout;

    @BeforeEach
    @DisplayName("User DAO tests are setting up")
    void beforeUserTests() {
        mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        stubNickname = "nickname";
        stubEmail = "email";
        stubFullname = "fullname";
        stubAbout = "about";
    }

    @Test
    @DisplayName("Change user #1: ALL NONE")
    void testChange1() {
        User user = new User(stubNickname, null, null, null);
        UserDAO.Change(user);

        verifyNoInteractions(mockJdbc);
    }

    @Test
    @DisplayName("Change user #2: Only Email")
    void testChange2() {
        User user = new User(stubNickname, stubEmail, null, null);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubEmail),
                Mockito.eq(stubNickname));
    }

    @Test
    @DisplayName("Change user #3: Only Fullname")
    void testChange3() {
        User user = new User(stubNickname, null, stubFullname, null);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubFullname),
                Mockito.eq(stubNickname));

    }

    @Test
    @DisplayName("Change user #4: Email + Fullname")
    void testChange4() {
        User user = new User(stubNickname, stubEmail, stubFullname, null);


        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubEmail),
                Mockito.eq(stubFullname),
                Mockito.eq(stubNickname));

    }

    @Test
    @DisplayName("Change user #5: Only About")
    void testChange5() {
        User user = new User(stubNickname, null, null, stubAbout);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubAbout),
                Mockito.eq(stubNickname));
    }

    @Test
    @DisplayName("Change user #6: Email + About")
    void testChange6() {
        User user = new User(stubNickname, stubEmail, null, stubAbout);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubEmail),
                Mockito.eq(stubAbout),
                Mockito.eq(stubNickname));
    }

    @Test
    @DisplayName("Change user #7: Fullname + About")
    void testChange7() {
        User user = new User(stubNickname, null, stubFullname, stubAbout);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubFullname),
                Mockito.eq(stubAbout),
                Mockito.eq(stubNickname));

    }

    @Test
    @DisplayName("Change user #8: Email + Fullname + About")
    void testChange8() {
        User user = new User(stubNickname, stubEmail, stubFullname, stubAbout);

        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(stubEmail),
                Mockito.eq(stubFullname),
                Mockito.eq(stubAbout),
                Mockito.eq(stubNickname)
        );

    }
}
