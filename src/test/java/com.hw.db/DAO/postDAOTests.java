package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import java.util.Date;
import java.sql.Timestamp;


public class postDAOTests {
    private JdbcTemplate mockJdbc;
    private int stubId;
    private Timestamp stubTime;
    private String stubAuthor;
    private String stubForum;
    private String stubMessage;
    private int stubParent;
    private int stubThread;


    @BeforeEach
    @DisplayName("User DAO tests are setting up")
    void beforePostTests() {
        mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);

        stubId = 1;
        Date date = new Date();
        stubTime = new Timestamp(date.getTime());
        stubAuthor = "Artem";
        stubForum = "MLP";
        stubMessage = "YES";
        stubParent = 2;
        stubThread = 3;

        Post stubPost = new Post("Farit", new Timestamp(0), stubForum, "MLP IS COOL!!!", stubParent - 1, stubThread, false);

        when(mockJdbc.queryForObject(
                eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                eq(stubId))).thenReturn(stubPost);
    }

    @Test
    @DisplayName("Set Post #1: ALL NONE")
    void testSetPost1() {
        Post post = new Post(null, null, stubForum, null, stubParent, stubThread, false);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(stubId));

        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("Set Post #2: Author")
    void testSetPost2() {
        Post post = new Post(stubAuthor, null, stubForum, null, stubParent, stubThread, false);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(stubAuthor),
                Mockito.eq(stubId)
        );
    }

    @Test
    @DisplayName("Set Post #3: Time")
    void testSetPost3() {
        Post post = new Post(null, stubTime, stubForum, null, stubParent, stubThread, false);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(stubTime),
                Mockito.eq(stubId)
        );
    }

    @Test
    @DisplayName("Set Post #4: Author + Time")
    void testSetPost4() {
        Post post = new Post(stubAuthor, stubTime, stubForum, null, stubParent, stubThread, true);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(stubAuthor),
                Mockito.eq(stubTime),
                Mockito.eq(stubId)
        );
    }

    @Test
    @DisplayName("Set Post #5: Message")
    void testSetPost5() {
        Post post = new Post(null, null, stubForum, stubMessage, stubParent, stubThread, false);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(stubMessage),
                Mockito.eq(stubId)
        );
    }

    @Test
    @DisplayName("Set Post #6: Author + Message")
    void testSetPost6() {
        Post post = new Post(stubAuthor, null, stubForum, stubMessage, stubParent, stubThread, true);

        PostDAO.setPost(stubId, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(stubAuthor),
                Mockito.eq(stubMessage),
                Mockito.eq(stubId)
        );
    }

}
