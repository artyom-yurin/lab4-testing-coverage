package com.hw.db.DAO;



import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class forumDAOTests {
    private JdbcTemplate mockJdbc;
    private String stubSlug;
    private String stubSince;
    private ArrayList<Object> conditions;

    @BeforeEach
    @DisplayName("Forum DAO tests are setting up")
    void beforeForumTests() {
        mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        stubSlug = "mlp";
        stubSince = "1-JAN-2020";
        conditions = new ArrayList<>();
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User List: stub")
    void testUserList1() {
        conditions.add(stubSlug);

        ForumDAO.UserList(stubSlug,null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User List: stub + since")
    void testUserList2() {
        conditions.add(stubSlug);
        conditions.add(stubSince);

        ForumDAO.UserList(stubSlug,null, stubSince, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User List: stub + since + desc + limit")
    void testUserList3() {
        conditions.add(stubSlug);
        conditions.add(stubSince);
        conditions.add(2);

        ForumDAO.UserList(stubSlug,2, stubSince, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));
    }



}
