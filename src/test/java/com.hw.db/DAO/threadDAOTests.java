package com.hw.db.DAO;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class threadDAOTests {
    private JdbcTemplate mockJdbc;
    private int stubId;
    private int stubLimit;
    private int stubSince;

    @BeforeEach
    @DisplayName("Thread DAO tests are setting up")
    void beforeThreadTests() {
        mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        stubId = 1;
        stubLimit = 2;
        stubSince = 3;
    }

    @Test
    @DisplayName("Tree Sort #1: since")
    void testTreeSort1() {
        ThreadDAO.treeSort(stubId, null, stubSince, null);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(stubId),
                Mockito.eq(stubSince)
        );

    }

    @Test
    @DisplayName("Tree Sort #2: limit + since + desc")
    void testTreeSort2() {
        ThreadDAO.treeSort(stubId, stubLimit, stubSince, true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(stubId),
                Mockito.eq(stubSince),
                Mockito.eq(stubLimit)
        );

    }

}
